<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {

	public $hasMany = array(
	'Post' => array(
		'className' => 'Post',
		'foreignKey' => 'user_id',
		'order' => 'created DESC',
	'Comment' => array(
		'className' => 'Comment',
		'foreignKey' => 'user_id',
		'order' => 'created DESC'
	)
	)
);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Username is required'
			)
			),
		'password' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Password is required'
			),
			'Match passwords' => array(
				'rule' => 'matchPasswords',
				'message' => 'Your password do bot match'
			)
			),
			'password_confirmation' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Password is required'
				)
				),
			
	);

	public function matchPasswords($data){
		if($data['password'] == $this->data['User']['password_confirmation']) {
			return true;
		}
		$this->invalidate('password_confirmation', 'Your password do not match');
		return false;
	}

	public function beforeSave($id=null)
	{
		if(isset($this->data['User']['password'])) {
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		}
		return true;
	}
}
