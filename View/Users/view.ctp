<div class="users view">
<h2><?php echo __('User'); ?></h2>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
	<?php if ($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin'): ?>
	<?php
		echo $this->Form->create('Post');
		echo $this->Form->hidden('user_id',array('value' => $user['User']['id']));
		echo $this->Form->input('title');
		echo $this->Form->input('body');
		echo $this->Form->end('Add Post');
	endif;?>
	</div>
</div>

<div class="col-sm-12">
	<div class="card">
	<?php foreach($post as $posts):?>
		<div class="card-header bg-info">			
			<h3><?php echo h($posts['Post']['title']);?></h3>
		</div>

		<div class="card-body">
			<h6><?php echo h($posts['Post']['body']);?></h6>
			<p>
				<?php echo __('By: ');?><?php echo h($user['User']['username']);?>
				&nbsp; 
			<?php echo $posts['Post']['created'];?>
			</p>
		</div>

		<div class="card-footer">
				<?php foreach($posts['Comment'] as $comment):?>	
				<p><?php echo h($comment['body']);?></p>
				<p>
					<?php echo __('By: ');?>
					<?php echo h($comment['username']);?>
					&nbsp; &nbsp;
					<?php echo $comment['created'];?></p>
				<hr>
				<?php endforeach;?>
		</div>

		<div class="card-body">
		<?php
					echo $this->Form->create('Comment');
					echo $this->Form->hidden('post_id',array('value' => $posts['Post']['id']));
					echo $this->Form->hidden('user_id',array('value' => $user['User']['id']));
					echo $this->Form->hidden('username',array('value' => $user['User']['username']));?>					
					<div class="input textarea">
						<label for="CommentBody"></label>
						<input name="data[Comment][body]" cols="30" rows="6" id="CommentBody" >
					</div>
					<?php echo $this->Form->end('Add Comment');?>
					
	</div>

		<?php endforeach;?>	
	</div>

	
	
</div>
</div>
</div>

<div class="actions">
<div class="row">
	<div class="col-sm-12">
		<dt><?php echo __('Username'); ?> :&nbsp; &nbsp;<?php echo h($user['User']['username']); ?></dt>
		<br>	
		<dt><?php echo __('Role'); ?> :&nbsp; &nbsp;<?php echo h($user['User']['role']); ?></dt>
		<br>
		<dt><?php echo __('Created'); ?><br> : <?php echo h($user['User']['created']); ?></dt>
		<br>
		<dt><?php echo __('Modified'); ?> <br> : <?php echo h($user['User']['modified']); ?></dt>
		<br>
		</div>

		<div class="col-sm-12">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
			<?php if ($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin'): ?>
				<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
				<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?> </li>
				<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
	<?php endif; ?>	
	</ul>
	</div>
	</div>
	
</div>


