<div class="row">
    <div class="col-sm-2"></div>
    
    <div class="col-sm-8" style="margin-top:10%">
        <div class="card">
            <div class="card-body">
            <div class="card-header">
                <div style="text-align:center;">
                    <h1>Welcome <span class="badge badge-secondary">Start up</span></h1>
                </div>
            </div>
            <div class="card-body">
                    <?php
                     echo $this->Form->create();?>

                    <div class="input text required">   
                        <label for="UserUsername">Username</label>
                        <div class="input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                <input name="data[User][username]" maxlength="50" type="text" id="UserUsername" required="required">
                            </div>              
                        </div> 
                    </div>      
    

                    <div class="input password required" style="margin-top:-5%">
                        <label for="UserPassword">Password</label>
                        <div class="input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                <input name="data[User][password]" type="password" id="UserPassword" required="required">
                            </div>              
                        </div>  
                    </div>
            </div>
            <div class="card-footer">
                    <div style="text-align:center;">
                        <button type="submit" class="btn btn-outline-success btn-lg">Login</button>
                    </form>
                    </div>
                    </div>
   
            </div>
        </div>
    </div>
</div>







  